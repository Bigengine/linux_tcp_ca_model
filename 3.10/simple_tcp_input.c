/*************************************************************************
    > File Name: simple_tcp_input.c
  > Author: wsun
  > Mail:sunweiflyus@gmail.com 
  > Created Time: Wed 07 Jun 2017 03:53:17 PM CDT
  > Comments: a simple version used for symbolic execution
 ************************************************************************/
#include "share.h"

static void tcp_cong_avoid(struct sock *sk, u32 ack, u32 in_flight)
{
	const struct inet_connection_sock *icsk = inet_csk(sk);
	icsk->icsk_ca_ops->cong_avoid(sk, ack, in_flight);
	tcp_sk(sk)->snd_cwnd_stamp = tcp_time_stamp;
}
