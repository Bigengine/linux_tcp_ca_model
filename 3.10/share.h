/*************************************************************************
    > File Name: share.h
  > Author: Wei Sun
  > Mail:sunweiflyus@gmail.com 
  > Created Time: Sat 27 May 2017 05:08:09 PM CDT
  > Comments:  some stub functions to close the environment of congestion control
 ************************************************************************/

#ifndef __SHARE_H__
#define __SHARE_H__
#define __read_mostly
#define __always_inline   inline
#define HZ 1000
#define MSEC_PER_SEC   1000L
#define USEC_PER_MSEC  1000L
#define BITS_PER_LONG  64
#include <linux/types.h>
#include <stddef.h>
unsigned long volatile jiffies = 0;
#define __init
#define tcp_time_stamp   ((__u32)(jiffies))
#define BUILD_BUG_ON(x)
#define THIS_MODULE ((struct module *)0)
#define likely(x) x
#define unlikely(x) x
#define pr_err_once(fmt, ...)
typedef unsigned char 		u8;
typedef unsigned short 		u16;
typedef unsigned int 		u32;
typedef char 				s8;
typedef short 				s16;
typedef int 				s32;

#ifdef __GNUC__
typedef long long s64;
typedef unsigned long long u64;
#else
typedef long long s64;
typedef unsigned long long u64;
#endif

#include "fls.h"
#include "__fls.h"
#include "fls64.h"

/*
 * min()/max()/clamp() macros that also do
 * strict type-checking.. See the
 * "unnecessary" pointer comparison.
 */
#define min(x, y) ({				\
	typeof(x) _min1 = (x);			\
	typeof(y) _min2 = (y);			\
	(void) (&_min1 == &_min2);		\
	_min1 < _min2 ? _min1 : _min2; })

#define max(x, y) ({				\
	typeof(x) _max1 = (x);			\
	typeof(y) _max2 = (y);			\
	(void) (&_max1 == &_max2);		\
	_max1 > _max2 ? _max1 : _max2; })

struct sk_buff{


};

/* This original do_div() is not C FUNCTION and is assmble language which klee cannot support;
 * To convert it to glibc implmenation, also this function only called withou any symbolic variables;
 */

#define do_div(n, base) div(&n, base);

u32
div (u64* numer, int denom)
{
 
  u64 res  = *numer / denom;
  u32 rem = *numer % denom;
  //printf("res:%llu\n",res);
  *numer = res;
  //printf("number:%llu\n",*numer);
  return rem;
}


/**
 * clamp - return a value clamped to a given range with strict typechecking
 * @val: current value
 * @min: minimum allowable value
 * @max: maximum allowable value
 *
 * This macro does strict typechecking of min/max to make sure they are of the
 * same type as val.  See the unnecessary pointer comparisons.
 */
#define clamp(val, min, max) ({			\
	typeof(val) __val = (val);		\
	typeof(min) __min = (min);		\
	typeof(max) __max = (max);		\
	(void) (&__val == &__min);		\
	(void) (&__val == &__max);		\
	__val = __val < __min ? __min: __val;	\
	__val > __max ? __max: __val; })


#if BITS_PER_LONG == 64
/**
 * div_u64_rem - unsigned 64bit divide with 32bit divisor with remainder
 *
 * This is commonly provided by 32bit archs to provide an optimized 64bit
 * divide.
 */
static inline u64 div_u64_rem(u64 dividend, u32 divisor, u32 *remainder)
{
	*remainder = dividend % divisor;
	return dividend / divisor;
}

#elif BITS_PER_LONG == 32
#ifndef div_u64_rem
static inline u64 div_u64_rem(u64 dividend, u32 divisor, u32 *remainder)
{
//	*remainder = do_div(dividend, divisor); // defalut is 64 bit
	return dividend;
}
#endif
#endif /* BITS_PER_LONG */

/**
 * div_u64 - unsigned 64bit divide with 32bit divisor
 *
 * This is the most common 64bit divide and should be used if possible,
 * as many 32bit archs can optimize this variant better than a full 64bit
 * divide.
 */
#ifndef div_u64
static inline u64 div_u64(u64 dividend, u32 divisor)
{
	u32 remainder;
	return div_u64_rem(dividend, divisor, &remainder);
}
#endif

/**
 * div64_u64 - unsigned 64bit divide with 64bit divisor
 * @dividend:	64bit dividend
 * @divisor:	64bit divisor
 *
 * This implementation is a modified version of the algorithm proposed
 * by the book 'Hacker's Delight'.  The original source and full proof
 * can be found here and is available for use without restriction.
 *
 * 'http://www.hackersdelight.org/HDcode/newCode/divDouble.c.txt'
 */
u64 div64_u64(u64 dividend, u64 divisor)
{
	u32 high = divisor >> 32;
	u64 quot;

	if (high == 0) {
		quot = div_u64(dividend, divisor);
	} else {
		int n = 1 + fls(high);
		quot = div_u64(dividend >> n, divisor >> n);

		if (quot != 0)
			quot--;
		if ((dividend - quot * divisor) >= divisor)
			quot++;
	}

	return quot;
}

/*
 * Convert jiffies to milliseconds and back.
 *
 * Avoid unnecessary multiplications/divisions in the
 * two most common HZ cases:
 */
unsigned int jiffies_to_msecs(const unsigned long j)
{
	return (MSEC_PER_SEC / HZ) * j; // only for HZ ==1000
}

#define MAX_JIFFY_OFFSET ((LONG_MAX >> 1)-1)
#define LONG_MAX ((long)(~0UL>>1))
// only consider HZ=1000 cases
unsigned long msecs_to_jiffies(const unsigned int m)
{
	/*
	 * Negative value, means infinite timeout:
	 */
	if ((int)m < 0)
		return MAX_JIFFY_OFFSET;
	/*
	 * HZ is equal to or smaller than 1000, and 1000 is a nice
	 * round multiple of HZ, divide with the factor between them,
	 * but round upwards:
	 */
	return (m + (MSEC_PER_SEC / HZ) - 1) / (MSEC_PER_SEC / HZ);

}
#include "simple_sock.h"
#include "simple_inet_connection.h"
#include "simple_tcp.h"
#include "simple_tcp_input.c"
#include "simple_tcp_cong.c"

#endif

