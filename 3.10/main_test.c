/*************************************************************************
    > File Name: main_test.c
  > Author: wsun
  > Mail:sunweiflyus@gmail.com 
  > Created Time: Fri 16 Jun 2017 01:06:14 PM CDT
  > Comments: 
 ************************************************************************/
#include "share.h"
#include "klee.h"
#include "tcp_cubic.c"

struct tcp_sock tcp_sock_struct ;
int main()
{
	cubictcp_register();
	memset(&tcp_sock_struct, 0, sizeof(struct tcp_sock));
	struct tcp_sock* sk = &tcp_sock_struct;
	tcp_set_congestion_control(sk, &cubictcp);//To bind ca with sk and initialize ca 
	tcp_init_sock(sk);
	struct bictcp *ca = inet_csk_ca(sk);
	struct tcp_sock *tp = tcp_sk(sk);
	//do init above

	//event 1: packet loss, window reduction
	klee_warning("Step 1: begin !!!");
	u32 last_max_cwnd = 0;
  	klee_make_symbolic(&last_max_cwnd, sizeof(u32), "last_max_cwnd");
	ca->last_max_cwnd = last_max_cwnd;

	u32 cwnd_0 = 0; //symbolic cwnd at step 1
  	klee_make_symbolic(&cwnd_0, sizeof(u32), "cwnd_0");
	tp->snd_cwnd = cwnd_0;
	klee_assume(cwnd_0 != 0);
	tp->snd_ssthresh = bictcp_recalc_ssthresh(sk);

	//event 2: On an ACK
	klee_warning("Step 2: begin !!!");
	int ack = 0;
	u32 prior_in_flights = 0;
  	klee_make_symbolic(&prior_in_flights, sizeof(prior_in_flights), "prior_in_flights");
	u32 cwnd = 0; //symbolic cwnd at step 2
  	klee_make_symbolic(&cwnd, sizeof(u32), "cwnd");
	tp->snd_cwnd = cwnd;

	s32 rtt_us = 0;
  	klee_make_symbolic(&rtt_us, sizeof(rtt_us), "rtt_us");
	u32 cnt = 1;
	bictcp_acked(sk, cnt, rtt_us);

	u32 epoch_start = 0;
 	klee_make_symbolic(&epoch_start, sizeof(u32), "epoach_start");
	ca->epoch_start = epoch_start;

	u32  ack_cnt = 0;
 	klee_make_symbolic(&ack_cnt, sizeof(u32), "ack_cnt");
	ca->ack_cnt = ack_cnt;

	u32 sym_jiffies = 0;
 	klee_make_symbolic(&sym_jiffies, sizeof(u32), "sym_jiffies");
	jiffies = sym_jiffies;
	klee_assume(epoch_start < sym_jiffies);

	tcp_cong_avoid(sk, ack, prior_in_flights);
	
	if(ca->cnt == 1 ) 
	{
   	  klee_warning("ca->cnt == 1");//detect an counterexample
	}
}
