/*************************************************************************
  > File Name: simple_tcp_input.c
  > Author: wsun
  > Mail:sunweiflyus@gmail.com
  > Created Time: Fri 23 Jun 2017 01:18:22 PM CDT
  > Comments:
 ************************************************************************/
#ifndef simple_tcp_input
#define simple_tcp_input
#include "share.h"
int sysctl_tcp_timestamps __read_mostly = 1;
int sysctl_tcp_window_scaling __read_mostly = 1;
int sysctl_tcp_sack __read_mostly = 1;
int sysctl_tcp_fack __read_mostly;
int sysctl_tcp_max_reordering __read_mostly = 300;
int sysctl_tcp_dsack __read_mostly = 1;
int sysctl_tcp_app_win __read_mostly = 31;
int sysctl_tcp_adv_win_scale __read_mostly = 1;
EXPORT_SYMBOL(sysctl_tcp_adv_win_scale);

/* rfc5961 challenge ack rate limiting */
int sysctl_tcp_challenge_ack_limit = 1000;

int sysctl_tcp_stdurg __read_mostly;
int sysctl_tcp_rfc1337 __read_mostly;
int sysctl_tcp_max_orphans __read_mostly = NR_FILE;
int sysctl_tcp_frto __read_mostly = 2;
int sysctl_tcp_min_rtt_wlen __read_mostly = 300;
int sysctl_tcp_moderate_rcvbuf __read_mostly = 1;
int sysctl_tcp_early_retrans __read_mostly = 3;
int sysctl_tcp_invalid_ratelimit __read_mostly = HZ/2;

#define FLAG_DATA		0x01 /* Incoming frame contained data.		*/
#define FLAG_WIN_UPDATE		0x02 /* Incoming ACK was a window update.	*/
#define FLAG_DATA_ACKED		0x04 /* This ACK acknowledged new data.		*/
#define FLAG_RETRANS_DATA_ACKED	0x08 /* "" "" some of which was retransmitted.	*/
#define FLAG_SYN_ACKED		0x10 /* This ACK acknowledged SYN.		*/
#define FLAG_DATA_SACKED	0x20 /* New SACK.				*/
#define FLAG_ECE		0x40 /* ECE in this ACK				*/
#define FLAG_LOST_RETRANS	0x80 /* This ACK marks some retransmission lost */
#define FLAG_SLOWPATH		0x100 /* Do not skip RFC checks for window update.*/
#define FLAG_ORIG_SACK_ACKED	0x200 /* Never retransmitted data are (s)acked	*/
#define FLAG_SND_UNA_ADVANCED	0x400 /* Snd_una was changed (!= FLAG_DATA_ACKED) */
#define FLAG_DSACKING_ACK	0x800 /* SACK blocks contained D-SACK info */
#define FLAG_SACK_RENEGING	0x2000 /* snd_una advanced to a sacked seq */
#define FLAG_UPDATE_TS_RECENT	0x4000 /* tcp_replace_ts_recent() */

#define FLAG_ACKED		(FLAG_DATA_ACKED|FLAG_SYN_ACKED)
#define FLAG_NOT_DUP		(FLAG_DATA|FLAG_WIN_UPDATE|FLAG_ACKED)
#define FLAG_CA_ALERT		(FLAG_DATA_SACKED|FLAG_ECE)
#define FLAG_FORWARD_PROGRESS	(FLAG_ACKED|FLAG_DATA_SACKED)

#define TCP_REMNANT (TCP_FLAG_FIN|TCP_FLAG_URG|TCP_FLAG_SYN|TCP_FLAG_PSH)
#define TCP_HP_BITS (~(TCP_RESERVED_BITS|TCP_FLAG_PSH))

#define REXMIT_NONE	0 /* no loss recovery to do */
#define REXMIT_LOST	1 /* retransmit packets marked lost */
#define REXMIT_NEW	2 /* FRTO-style transmit of unsent/new packets */

static void tcp_gro_dev_warn(struct sock *sk, const struct sk_buff *skb,
		unsigned int len)
{

	STUB(1, "the stub function is called !\n");
	return;
}

/* Adapt the MSS value used to make delayed ack decision to the
 * real world.
 */
static void tcp_measure_rcv_mss(struct sock *sk, const struct sk_buff *skb)
{

	STUB(1, "the stub function is called !\n");
	return;
}

static void tcp_incr_quickack(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return;
}

static void tcp_enter_quickack_mode(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return;
}

/* Send ACKs quickly, if "quick" count is not exhausted
 * and the session is not interactive.
 */

static bool tcp_in_quickack_mode(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return false;
}

static void tcp_ecn_queue_cwr(struct tcp_sock *tp)
{

	STUB(1, "the stub function is called !\n");
	return;
}

static void tcp_ecn_accept_cwr(struct tcp_sock *tp, const struct sk_buff *skb)
{

	STUB(1, "the stub function is called !\n");
	return;
}

static void tcp_ecn_withdraw_cwr(struct tcp_sock *tp)
{

	STUB(1, "the stub function is called !\n");
	return;
}

static void __tcp_ecn_check_ce(struct tcp_sock *tp, const struct sk_buff *skb)
{

	STUB(1, "the stub function is called !\n");
	return;
}


static void tcp_sndbuf_expand(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return;
}


/* Set the sk_pacing_rate to allow proper sizing of TSO packets.
 * Note: TCP stack does not yet implement pacing.
 * FQ packet scheduler can be used to implement cheap but effective
 * TCP pacing, to smooth the burst on large writes when packets
 * in flight is significantly lower than cwnd (or rwin)
 */
	int sysctl_tcp_pacing_ss_ratio __read_mostly = 200;
	int sysctl_tcp_pacing_ca_ratio __read_mostly = 120;

static void tcp_update_pacing_rate(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return ;
}

/* Calculate rto without backoff.  This is the second half of Van Jacobson's
 * routine referred to above.
 */
static void tcp_set_rto(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return;
}

__u32 tcp_init_cwnd(const struct tcp_sock *tp, const struct dst_entry *dst)
{
	__u32 cwnd =  0;

	if (!cwnd)
		cwnd = TCP_INIT_CWND;
	return min_t(__u32, cwnd, tp->snd_cwnd_clamp);
}

/*
 * Packet counting of FACK is based on in-order assumptions, therefore TCP
 * disables it when reordering is detected
 */
void tcp_disable_fack(struct tcp_sock *tp)
{
	/* RFC3517 uses different metric in lost marker => reset on change */
	if (tcp_is_fack(tp))
		tp->lost_skb_hint = NULL;
	tp->rx_opt.sack_ok &= ~TCP_FACK_ENABLED;
}

/* Take a notice that peer is sending D-SACKs */
static void tcp_dsack_seen(struct tcp_sock *tp)
{
	tp->rx_opt.sack_ok |= TCP_DSACK_SEEN;
}

static void tcp_update_reordering(struct sock *sk, const int metric,
		const int ts)
{
	struct tcp_sock *tp = tcp_sk(sk);
	int mib_idx;

	if (metric > tp->reordering) {
		tp->reordering = min(sysctl_tcp_max_reordering, metric);

		/*#if FASTRETRANS_DEBUG > 1*/
		/*pr_debug("Disorder%d %d %u f%u s%u rr%d\n",*/
		/*tp->rx_opt.sack_ok, inet_csk(sk)->icsk_ca_state,*/
		/*tp->reordering,*/
		/*tp->fackets_out,*/
		/*tp->sacked_out,*/
		/*tp->undo_marker ? tp->undo_retrans : 0);*/
		/*#endif*/
		tcp_disable_fack(tp);
	}

	tp->rack.reord = 1;

	/* This exciting event is worth to be remembered. 8) */
	/*
	   if (ts)
	   mib_idx = LINUX_MIB_TCPTSREORDER;
	   else if (tcp_is_reno(tp))
	   mib_idx = LINUX_MIB_TCPRENOREORDER;
	   else if (tcp_is_fack(tp))
	   mib_idx = LINUX_MIB_TCPFACKREORDER;
	   else
	   mib_idx = LINUX_MIB_TCPSACKREORDER;

	   NET_INC_STATS(sock_net(sk), mib_idx);
	   */
}

/* This must be called before lost_out is incremented */
static void tcp_verify_retransmit_hint(struct tcp_sock *tp, struct sk_buff *skb)
{

	STUB(1, "the stub function is called !\n");
	return;
}

/* Sum the number of packets on the wire we have marked as lost.
 * There are two cases we care about here:
 * a) Packet hasn't been marked lost (nor retransmitted),
 *    and this is the first loss.
 * b) Packet has been marked both lost and retransmitted,
 *    and this means we think it was lost again.
 */
static void tcp_sum_lost(struct tcp_sock *tp, struct sk_buff *skb)
{
	STUB(1, "the stub function is called !\n");
	return;
}

static void tcp_skb_mark_lost(struct tcp_sock *tp, struct sk_buff *skb)
{

	STUB(1, "the stub function is called !\n");
	return;
}

void tcp_skb_mark_lost_uncond_verify(struct tcp_sock *tp, struct sk_buff *skb)
{

	STUB(1, "the stub function is called !\n");
	return;
}

void tcp_clear_retrans(struct tcp_sock *tp)
{
	tp->retrans_out = 0;
	tp->lost_out = 0;
	tp->undo_marker = 0;
	tp->undo_retrans = -1;
	tp->fackets_out = 0;
	tp->sacked_out = 0;
}

static inline void tcp_init_undo(struct tcp_sock *tp)
{
	tp->undo_marker = tp->snd_una;
	/* Retransmission still in flight may cause DSACKs later. */
	tp->undo_retrans = tp->retrans_out ? : -1;
}

/* Enter Loss state. If we detect SACK reneging, forget all SACK information
 * and reset tags completely, otherwise preserve SACKs. If receiver
 * dropped its ofo queue, we will know this due to reneging detection.
 */
void tcp_enter_loss(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return ;
}

/* If ACK arrived pointing to a remembered SACK, it means that our
 * remembered SACKs do not reflect real state of receiver i.e.
 * receiver _host_ is heavily congested (or buggy).
 *
 * To avoid big spurious retransmission bursts due to transient SACK
 * scoreboard oddities that look like reneging, we give the receiver a
 * little time (max(RTT/2, 10ms)) to send us some more ACKs that will
 * restore sanity to the SACK scoreboard. If the apparent reneging
 * persists until this RTO then we'll clear the SACK scoreboard.
 */
static bool tcp_check_sack_reneging(struct sock *sk, int flag)
{
	STUB(1, "the stub function is called !\n");
	return false;
}

static inline int tcp_fackets_out(const struct tcp_sock *tp)
{
	return tcp_is_reno(tp) ? tp->sacked_out + 1 : tp->fackets_out;
}

/* Heurestics to calculate number of duplicate ACKs. There's no dupACKs
 * counter when SACK is enabled (without SACK, sacked_out is used for
 * that purpose).
 *
 * Instead, with FACK TCP uses fackets_out that includes both SACKed
 * segments up to the highest received SACK block so far and holes in
 * between them.
 *
 * With reordering, holes may still be in flight, so RFC3517 recovery
 * uses pure sacked_out (total number of SACKed segments) even though
 * it violates the RFC that uses duplicate ACKs, often these are equal
 * but when e.g. out-of-window ACKs or packet duplication occurs,
 * they differ. Since neither occurs due to loss, TCP should really
 * ignore them.
 */
static inline int tcp_dupack_heuristics(const struct tcp_sock *tp)
{
	return tcp_is_fack(tp) ? tp->fackets_out : tp->sacked_out + 1;
}

/* Linux NewReno/SACK/FACK/ECN state machine.
 * --------------------------------------
 *
 * "Open"	Normal state, no dubious events, fast path.
 * "Disorder"   In all the respects it is "Open",
 *		but requires a bit more attention. It is entered when
 *		we see some SACKs or dupacks. It is split of "Open"
 *		mainly to move some processing from fast path to slow one.
 * "CWR"	CWND was reduced due to some Congestion Notification event.
 *		It can be ECN, ICMP source quench, local device congestion.
 * "Recovery"	CWND was reduced, we are fast-retransmitting.
 * "Loss"	CWND was reduced due to RTO timeout or SACK reneging.
 *
 * tcp_fastretrans_alert() is entered:
 * - each incoming ACK, if state is not "Open"
 * - when arrived ACK is unusual, namely:
 *	* SACK
 *	* Duplicate ACK.
 *	* ECN ECE.
 *
 * Counting packets in flight is pretty simple.
 *
 *	in_flight = packets_out - left_out + retrans_out
 *
 *	packets_out is SND.NXT-SND.UNA counted in packets.
 *
 *	retrans_out is number of retransmitted segments.
 *
 *	left_out is number of segments left network, but not ACKed yet.
 *
 *		left_out = sacked_out + lost_out
 *
 *     sacked_out: Packets, which arrived to receiver out of order
 *		   and hence not ACKed. With SACKs this number is simply
 *		   amount of SACKed data. Even without SACKs
 *		   it is easy to give pretty reliable estimate of this number,
 *		   counting duplicate ACKs.
 *
 *       lost_out: Packets lost by network. TCP has no explicit
 *		   "loss notification" feedback from network (for now).
 *		   It means that this number can be only _guessed_.
 *		   Actually, it is the heuristics to predict lossage that
 *		   distinguishes different algorithms.
 *
 *	F.e. after RTO, when all the queue is considered as lost,
 *	lost_out = packets_out and in_flight = retrans_out.
 *
 *		Essentially, we have now a few algorithms detecting
 *		lost packets.
 *
 *		If the receiver supports SACK:
 *
 *		RFC6675/3517: It is the conventional algorithm. A packet is
 *		considered lost if the number of higher sequence packets
 *		SACKed is greater than or equal the DUPACK thoreshold
 *		(reordering). This is implemented in tcp_mark_head_lost and
 *		tcp_update_scoreboard.
 *
 *		RACK (draft-ietf-tcpm-rack-01): it is a newer algorithm
 *		(2017-) that checks timing instead of counting DUPACKs.
 *		Essentially a packet is considered lost if it's not S/ACKed
 *		after RTT + reordering_window, where both metrics are
 *		dynamically measured and adjusted. This is implemented in
 *		tcp_rack_mark_lost.
 *
 *		FACK (Disabled by default. Subsumbed by RACK):
 *		It is the simplest heuristics. As soon as we decided
 *		that something is lost, we decide that _all_ not SACKed
 *		packets until the most forward SACK are lost. I.e.
 *		lost_out = fackets_out - sacked_out and left_out = fackets_out.
 *		It is absolutely correct estimate, if network does not reorder
*		packets. And it loses any connection to reality when reordering
		 *		takes place. We use FACK by default until reordering
		  *		is suspected on the path to this destination.
		  *
		  *		If the receiver does not support SACK:
		  *
		  *		NewReno (RFC6582): in Recovery we assume that one segment
								   *		is lost (classic Reno). While we are in Recovery and
														*		a partial ACK arrives, we assume that one more packet
																		 *		is lost (NewReno). This heuristics are the same in NewReno
																					*		and SACK.
																							  *
																							   * Really tricky (and requiring careful tuning) part of algorithm
																							   * is hidden in functions tcp_time_to_recover() and tcp_xmit_retransmit_queue().
																							   * The first determines the moment _when_ we should reduce CWND and,
																							   * hence, slow down forward transmission. In fact, it determines the moment
																							   * when we decide that hole is caused by loss, rather than by a reorder.
																							   *
																							   * tcp_xmit_retransmit_queue() decides, _what_ we should retransmit to fill
																							   * holes, caused by lost packets.
																							   *
																							   * And the most logically complicated part of algorithm is undo
																							   * heuristics. We detect false retransmits due to both too early
																							   * fast retransmit (reordering) and underestimated RTO, analyzing
																							   * timestamps and D-SACKs. When we detect that some segments were
																							   * retransmitted by mistake and CWND reduction was wrong, we undo
																							   * window reduction and abort recovery phase. This logic is hidden
																							   * inside several functions named tcp_try_undo_<something>.
																							   */

																							   /* This function decides, when we should leave Disordered state
																								* and enter Recovery phase, reducing congestion window.
																								*
																								* Main question: may we further continue forward transmission
																								* with the same cwnd?
																								*/
static bool tcp_time_to_recover(struct sock *sk, int flag)
{
	struct tcp_sock *tp = tcp_sk(sk);

	/* Trick#1: The loss is proven. */
	if (tp->lost_out)
		return true;

	/* Not-A-Trick#2 : Classic rule... */
	if (tcp_dupack_heuristics(tp) > tp->reordering)
		return true;

	return false;
}

/* Detect loss in event "A" above by marking head of queue up as lost.
 * For FACK or non-SACK(Reno) senders, the first "packets" number of segments
 * are considered lost. For RFC3517 SACK, a segment is considered lost if it
 * has at least tp->reordering SACKed seqments above it; "packets" refers to
 * the maximum SACKed segments to pass before reaching this limit.
 */
static void tcp_mark_head_lost(struct sock *sk, int packets, int mark_head)
{

	STUB(1, "the stub function is called !\n");
	return;
}

/* Account newly detected lost packet(s) */

static void tcp_update_scoreboard(struct sock *sk, int fast_rexmit)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (tcp_is_reno(tp)) {
		tcp_mark_head_lost(sk, 1, 1);
	} else if (tcp_is_fack(tp)) {
		int lost = tp->fackets_out - tp->reordering;
		if (lost <= 0)
			lost = 1;
		tcp_mark_head_lost(sk, lost, 0);
	} else {
		int sacked_upto = tp->sacked_out - tp->reordering;
		if (sacked_upto >= 0)
			tcp_mark_head_lost(sk, sacked_upto, 0);
		else if (fast_rexmit)
			tcp_mark_head_lost(sk, 1, 1);
	}
}

#define DBGUNDO(x...) do { } while (0)

static void tcp_undo_cwnd_reduction(struct sock *sk, bool unmark_loss)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (unmark_loss) {
		struct sk_buff *skb;

		/*tcp_for_write_queue(skb, sk) {*/
		/*if (skb == tcp_send_head(sk))*/
		/*break;*/
		/*TCP_SKB_CB(skb)->sacked &= ~TCPCB_LOST;*/
		/*}*/
		tp->lost_out = 0;
		tcp_clear_all_retrans_hints(tp);
	}

	if (tp->prior_ssthresh) {
		const struct inet_connection_sock *icsk = inet_csk(sk);

		tp->snd_cwnd = icsk->icsk_ca_ops->undo_cwnd(sk);

		if (tp->prior_ssthresh > tp->snd_ssthresh) {
			tp->snd_ssthresh = tp->prior_ssthresh;
			tcp_ecn_withdraw_cwr(tp);
		}
	}
	tp->snd_cwnd_stamp = tcp_time_stamp;
	tp->undo_marker = 0;
}

static bool tcp_tsopt_ecr_before(const struct tcp_sock *tp, u32 when)
{
	return tp->rx_opt.saw_tstamp && tp->rx_opt.rcv_tsecr &&
		before(tp->rx_opt.rcv_tsecr, when);
}

static void tcp_add_reno_sack(struct sock *sk)
{
	STUB(1, "the stub function is called !\n");
	return;
}

static inline bool tcp_packet_delayed(const struct tcp_sock *tp)
{
	return !tp->retrans_stamp ||
		tcp_tsopt_ecr_before(tp, tp->retrans_stamp);
}

static inline bool tcp_may_undo(const struct tcp_sock *tp)
{
	return tp->undo_marker && (!tp->undo_retrans || tcp_packet_delayed(tp));
}
static bool tcp_any_retrans_done(const struct sock *sk)
{
	STUB(1, "the stub function is called !\n");
	return false;
}

/* People celebrate: "We love our President!" */
static bool tcp_try_undo_recovery(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (tcp_may_undo(tp)) {
		int mib_idx;

		/* Happy end! We did not retransmit anything
		 * or our original transmission succeeded.
		 */
		DBGUNDO(sk, inet_csk(sk)->icsk_ca_state == TCP_CA_Loss ? "loss" : "retrans");
		tcp_undo_cwnd_reduction(sk, false);
		if (inet_csk(sk)->icsk_ca_state == TCP_CA_Loss)
			mib_idx = LINUX_MIB_TCPLOSSUNDO;
		else
			mib_idx = LINUX_MIB_TCPFULLUNDO;

		NET_INC_STATS(sock_net(sk), mib_idx);
	}
	if (tp->snd_una == tp->high_seq && tcp_is_reno(tp)) {
		/* Hold old state until something *above* high_seq
		 * is ACKed. For Reno it is MUST to prevent false
		 * fast retransmits (RFC2582). SACK TCP is safe. */
		if (!tcp_any_retrans_done(sk))
			tp->retrans_stamp = 0;
		return true;
	}
	tcp_set_ca_state(sk, TCP_CA_Open);
	return false;
}

/* Try to undo cwnd reduction, because D-SACKs acked all retransmitted data */
static bool tcp_try_undo_dsack(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (tp->undo_marker && !tp->undo_retrans) {
		DBGUNDO(sk, "D-SACK");
		tcp_undo_cwnd_reduction(sk, false);
		NET_INC_STATS(sock_net(sk), LINUX_MIB_TCPDSACKUNDO);
		return true;
	}
	return false;
}

/* Undo during loss recovery after partial ACK or using F-RTO. */
static bool tcp_try_undo_loss(struct sock *sk, bool frto_undo)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (frto_undo || tcp_may_undo(tp)) {
		tcp_undo_cwnd_reduction(sk, true);

		DBGUNDO(sk, "partial loss");
		NET_INC_STATS(sock_net(sk), LINUX_MIB_TCPLOSSUNDO);
		if (frto_undo)
			NET_INC_STATS(sock_net(sk),
					LINUX_MIB_TCPSPURIOUSRTOS);
		inet_csk(sk)->icsk_retransmits = 0;
		if (frto_undo || tcp_is_sack(tp))
			tcp_set_ca_state(sk, TCP_CA_Open);
		return true;
	}
	return false;
}

/* The cwnd reduction in CWR and Recovery uses the PRR algorithm in RFC 6937.
 * It computes the number of packets to send (sndcnt) based on packets newly
 * delivered:
 *   1) If the packets in flight is larger than ssthresh, PRR spreads the
 *	cwnd reductions across a full RTT.
 *   2) Otherwise PRR uses packet conservation to send as much as delivered.
 *      But when the retransmits are acked without further losses, PRR
 *      slow starts cwnd up to ssthresh to speed up the recovery.
 */
static void tcp_init_cwnd_reduction(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);

	tp->high_seq = tp->snd_nxt;
	tp->tlp_high_seq = 0;
	tp->snd_cwnd_cnt = 0;
	tp->prior_cwnd = tp->snd_cwnd;
	tp->prr_delivered = 0;
	tp->prr_out = 0;
	tp->snd_ssthresh = inet_csk(sk)->icsk_ca_ops->ssthresh(sk);
	tcp_ecn_queue_cwr(tp);
}

void tcp_cwnd_reduction(struct sock *sk, int newly_acked_sacked, int flag)
{
	struct tcp_sock *tp = tcp_sk(sk);
	int sndcnt = 0;
	int delta = tp->snd_ssthresh - tcp_packets_in_flight(tp);

	if (newly_acked_sacked <= 0 || WARN_ON_ONCE(!tp->prior_cwnd))
		return;

	tp->prr_delivered += newly_acked_sacked;
	if (delta < 0) {
		u64 dividend = (u64)tp->snd_ssthresh * tp->prr_delivered +
			tp->prior_cwnd - 1;
		sndcnt = div_u64(dividend, tp->prior_cwnd) - tp->prr_out;
	} else if ((flag & FLAG_RETRANS_DATA_ACKED) &&
			!(flag & FLAG_LOST_RETRANS)) {
		sndcnt = min_t(int, delta,
				max_t(int, tp->prr_delivered - tp->prr_out,
					newly_acked_sacked) + 1);
	} else {
		sndcnt = min(delta, newly_acked_sacked);
	}
	/* Force a fast retransmit upon entering fast recovery */
	sndcnt = max(sndcnt, (tp->prr_out ? 0 : 1));
	tp->snd_cwnd = tcp_packets_in_flight(tp) + sndcnt;
}

static inline void tcp_end_cwnd_reduction(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (inet_csk(sk)->icsk_ca_ops->cong_control)
		return;

	/* Reset cwnd to ssthresh in CWR or Recovery (unless it's undone) */
	if (inet_csk(sk)->icsk_ca_state == TCP_CA_CWR ||
			(tp->undo_marker && tp->snd_ssthresh < TCP_INFINITE_SSTHRESH)) {
		tp->snd_cwnd = tp->snd_ssthresh;
		tp->snd_cwnd_stamp = tcp_time_stamp;
	}
	tcp_ca_event(sk, CA_EVENT_COMPLETE_CWR);
}

/* Enter CWR state. Disable cwnd undo since congestion is proven with ECN */
void tcp_enter_cwr(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);

	tp->prior_ssthresh = 0;
	if (inet_csk(sk)->icsk_ca_state < TCP_CA_CWR) {
		tp->undo_marker = 0;
		tcp_init_cwnd_reduction(sk);
		tcp_set_ca_state(sk, TCP_CA_CWR);
	}
}
EXPORT_SYMBOL(tcp_enter_cwr);

static void tcp_try_keep_open(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	int state = TCP_CA_Open;

	if (tcp_left_out(tp) || tcp_any_retrans_done(sk))
		state = TCP_CA_Disorder;

	if (inet_csk(sk)->icsk_ca_state != state) {
		tcp_set_ca_state(sk, state);
		tp->high_seq = tp->snd_nxt;
	}
}

static void tcp_try_to_open(struct sock *sk, int flag)
{
	struct tcp_sock *tp = tcp_sk(sk);

	tcp_verify_left_out(tp);

	if (!tcp_any_retrans_done(sk))
		tp->retrans_stamp = 0;

	if (flag & FLAG_ECE)
		tcp_enter_cwr(sk);

	if (inet_csk(sk)->icsk_ca_state != TCP_CA_CWR) {
		tcp_try_keep_open(sk);
	}
}

static void tcp_mtup_probe_failed(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return;
}

static void tcp_mtup_probe_success(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return;
}

/* Do a simple retransmit without using the backoff mechanisms in
 * tcp_timer. This is used for path mtu discovery.
 * The socket is already locked here.
 */
void tcp_simple_retransmit(struct sock *sk)
{

	STUB(1, "the stub function is called !\n");
	return ;
}
EXPORT_SYMBOL(tcp_simple_retransmit);

void tcp_enter_recovery(struct sock *sk, bool ece_ack)
{
	struct tcp_sock *tp = tcp_sk(sk);
	int mib_idx;

	if (tcp_is_reno(tp))
		mib_idx = LINUX_MIB_TCPRENORECOVERY;
	else
		mib_idx = LINUX_MIB_TCPSACKRECOVERY;

	NET_INC_STATS(sock_net(sk), mib_idx);

	tp->prior_ssthresh = 0;
	tcp_init_undo(tp);

	if (!tcp_in_cwnd_reduction(sk)) {
		if (!ece_ack)
			tp->prior_ssthresh = tcp_current_ssthresh(sk);
		tcp_init_cwnd_reduction(sk);
	}
	tcp_set_ca_state(sk, TCP_CA_Recovery);
}

static inline void tcp_reset_reno_sack(struct tcp_sock *tp)
{
	tp->sacked_out = 0;
}

/* Process an ACK in CA_Loss state. Move to CA_Open if lost data are
 * recovered or spurious. Otherwise retransmits more on partial ACKs.
 */
static void tcp_process_loss(struct sock *sk, int flag, bool is_dupack,
		int *rexmit)
{
	struct tcp_sock *tp = tcp_sk(sk);
	bool recovered = !before(tp->snd_una, tp->high_seq);

	if ((flag & FLAG_SND_UNA_ADVANCED) &&
			tcp_try_undo_loss(sk, false))
		return;

	/* The ACK (s)acks some never-retransmitted data meaning not all
	 * the data packets before the timeout were lost. Therefore we
	 * undo the congestion window and state. This is essentially
	 * the operation in F-RTO (RFC5682 section 3.1 step 3.b). Since
	 * a retransmitted skb is permantly marked, we can apply such an
	 * operation even if F-RTO was not used.
	 */
	if ((flag & FLAG_ORIG_SACK_ACKED) &&
			tcp_try_undo_loss(sk, tp->undo_marker))
		return;

	if (tp->frto) { /* F-RTO RFC5682 sec 3.1 (sack enhanced version). */
		if (after(tp->snd_nxt, tp->high_seq)) {
			if (flag & FLAG_DATA_SACKED || is_dupack)
				tp->frto = 0; /* Step 3.a. loss was real */
		} else if (flag & FLAG_SND_UNA_ADVANCED && !recovered) {
			tp->high_seq = tp->snd_nxt;
			/* Step 2.b. Try send new data (but deferred until cwnd
			 * is updated in tcp_ack()). Otherwise fall back to
			 * the conventional recovery.
			 */
			if (tcp_send_head(sk) &&
					after(tcp_wnd_end(tp), tp->snd_nxt)) {
				*rexmit = REXMIT_NEW;
				return;
			}
			tp->frto = 0;
		}
	}

	if (recovered) {
		/* F-RTO RFC5682 sec 3.1 step 2.a and 1st part of step 3.a */
		tcp_try_undo_recovery(sk);
		return;
	}
	if (tcp_is_reno(tp)) {
		/* A Reno DUPACK means new data in F-RTO step 2.b above are
		 * delivered. Lower inflight to clock out (re)tranmissions.
		 */
		if (after(tp->snd_nxt, tp->high_seq) && is_dupack)
			tcp_add_reno_sack(sk);
		else if (flag & FLAG_SND_UNA_ADVANCED)
			tcp_reset_reno_sack(tp);
	}
	*rexmit = REXMIT_LOST;
}

/* Undo during fast recovery after partial ACK. */
static bool tcp_try_undo_partial(struct sock *sk, const int acked)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (tp->undo_marker && tcp_packet_delayed(tp)) {
		/* Plain luck! Hole if filled with delayed
		 * packet, rather than with a retransmit.
		 */
		tcp_update_reordering(sk, tcp_fackets_out(tp) + acked, 1);

		/* We are getting evidence that the reordering degree is higher
		 * than we realized. If there are no retransmits out then we
		 * can undo. Otherwise we clock out new packets but do not
		 * mark more packets lost or retransmit more.
		 */
		if (tp->retrans_out)
			return true;

		if (!tcp_any_retrans_done(sk))
			tp->retrans_stamp = 0;

		DBGUNDO(sk, "partial recovery");
		tcp_undo_cwnd_reduction(sk, true);
		NET_INC_STATS(sock_net(sk), LINUX_MIB_TCPPARTIALUNDO);
		tcp_try_keep_open(sk);
		return true;
	}
	return false;
}

static void tcp_rack_identify_loss(struct sock *sk, int *ack_flag)
{
	/*struct tcp_sock *tp = tcp_sk(sk);*/

	/*[> Use RACK to detect loss <]*/
	/*if (sysctl_tcp_recovery & TCP_RACK_LOSS_DETECTION) {*/
	/*u32 prior_retrans = tp->retrans_out;*/

	/*tcp_rack_mark_lost(sk);*/
	/*if (prior_retrans > tp->retrans_out)*/
	/**ack_flag |= FLAG_LOST_RETRANS;*/
	/*}*/
	STUB(1, "the stub function is called !\n");
}

/* Process an event, which can update packets-in-flight not trivially.
 * Main goal of this function is to calculate new estimate for left_out,
 * taking into account both packets sitting in receiver's buffer and
 * packets lost by network.
 *
 * Besides that it updates the congestion state when packet loss or ECN
 * is detected. But it does not reduce the cwnd, it is done by the
 * congestion control later.
 *
 * It does _not_ decide what to send, it is made in function
 * tcp_xmit_retransmit_queue().
 */
static void tcp_fastretrans_alert(struct sock *sk, const int acked,
		bool is_dupack, int *ack_flag, int *rexmit)
{

	STUB(1, "the stub function is called !\n");
	/*struct inet_connection_sock *icsk = inet_csk(sk);*/
	/*struct tcp_sock *tp = tcp_sk(sk);*/
	/*int fast_rexmit = 0, flag = *ack_flag;*/
	/*bool do_lost = is_dupack || ((flag & FLAG_DATA_SACKED) &&*/
	/*(tcp_fackets_out(tp) > tp->reordering));*/

	/*if (WARN_ON(!tp->packets_out && tp->sacked_out))*/
	/*tp->sacked_out = 0;*/
	/*if (WARN_ON(!tp->sacked_out && tp->fackets_out))*/
	/*tp->fackets_out = 0;*/

	/* Now state machine starts.
	 * A. ECE, hence prohibit cwnd undoing, the reduction is required. */
	/*if (flag & FLAG_ECE)*/
	/*tp->prior_ssthresh = 0;*/

	/*[> B. In all the states check for reneging SACKs. <]*/
	/*if (tcp_check_sack_reneging(sk, flag))*/
	/*return;*/

	/*[> C. Check consistency of the current state. <]*/
	/*tcp_verify_left_out(tp);*/

	/* D. Check state exit conditions. State can be terminated
	 *    when high_seq is ACKed. */
	/*if (icsk->icsk_ca_state == TCP_CA_Open) {*/
	/*WARN_ON(tp->retrans_out != 0);*/
	/*tp->retrans_stamp = 0;*/
	/*} else if (!before(tp->snd_una, tp->high_seq)) {*/
	/*switch (icsk->icsk_ca_state) {*/
	/*case TCP_CA_CWR:*/
	/* CWR is to be held something *above* high_seq
	 * is ACKed for CWR bit to reach receiver. */
	/*if (tp->snd_una != tp->high_seq) {*/
	/*tcp_end_cwnd_reduction(sk);*/
	/*tcp_set_ca_state(sk, TCP_CA_Open);*/
	/*}*/
	/*break;*/

	/*case TCP_CA_Recovery:*/
	/*if (tcp_is_reno(tp))*/
	/*tcp_reset_reno_sack(tp);*/
	/*if (tcp_try_undo_recovery(sk))*/
	/*return;*/
	/*tcp_end_cwnd_reduction(sk);*/
	/*break;*/
	/*}*/
	/*}*/

	/*[> E. Process state. <]*/
	/*switch (icsk->icsk_ca_state) {*/
	/*case TCP_CA_Recovery:*/
	/*if (!(flag & FLAG_SND_UNA_ADVANCED)) {*/
	/*if (tcp_is_reno(tp) && is_dupack)*/
	/*tcp_add_reno_sack(sk);*/
	/*} else {*/
	/*if (tcp_try_undo_partial(sk, acked))*/
	/*return;*/
	/*[> Partial ACK arrived. Force fast retransmit. <]*/
	/*do_lost = tcp_is_reno(tp) ||*/
	/*tcp_fackets_out(tp) > tp->reordering;*/
	/*}*/
	/*if (tcp_try_undo_dsack(sk)) {*/
	/*tcp_try_keep_open(sk);*/
	/*return;*/
	/*}*/
	/*tcp_rack_identify_loss(sk, ack_flag);*/
	/*break;*/
	/*case TCP_CA_Loss:*/
	/*tcp_process_loss(sk, flag, is_dupack, rexmit);*/
	/*tcp_rack_identify_loss(sk, ack_flag);*/
	/*if (!(icsk->icsk_ca_state == TCP_CA_Open ||*/
	/*(*ack_flag & FLAG_LOST_RETRANS)))*/
	/*return;*/
	/*[> Change state if cwnd is undone or retransmits are lost <]*/
	/*default:*/
	/*if (tcp_is_reno(tp)) {*/
	/*if (flag & FLAG_SND_UNA_ADVANCED)*/
	/*tcp_reset_reno_sack(tp);*/
	/*if (is_dupack)*/
	/*tcp_add_reno_sack(sk);*/
	/*}*/

	/*if (icsk->icsk_ca_state <= TCP_CA_Disorder)*/
	/*tcp_try_undo_dsack(sk);*/

	/*tcp_rack_identify_loss(sk, ack_flag);*/
	/*if (!tcp_time_to_recover(sk, flag)) {*/
	/*tcp_try_to_open(sk, flag);*/
	/*return;*/
	/*}*/

	/*[> MTU probe failure: don't reduce cwnd <]*/
	/*if (icsk->icsk_ca_state < TCP_CA_CWR &&*/
	/*icsk->icsk_mtup.probe_size &&*/
	/*tp->snd_una == tp->mtu_probe.probe_seq_start) {*/
	/*tcp_mtup_probe_failed(sk);*/
	/*[> Restores the reduction we did in tcp_mtup_probe() <]*/
	/*tp->snd_cwnd++;*/
	/*tcp_simple_retransmit(sk);*/
	/*return;*/
	/*}*/

	/*[> Otherwise enter Recovery state <]*/
	/*tcp_enter_recovery(sk, (flag & FLAG_ECE));*/
	/*fast_rexmit = 1;*/
	/*}*/

	/*if (do_lost)*/
	/*tcp_update_scoreboard(sk, fast_rexmit);*/
	/**rexmit = REXMIT_LOST;*/
}



static void tcp_cong_avoid(struct sock *sk, u32 ack, u32 acked)
{
	const struct inet_connection_sock *icsk = inet_csk(sk);

	icsk->icsk_ca_ops->cong_avoid(sk, ack, acked);
	tcp_sk(sk)->snd_cwnd_stamp = tcp_time_stamp;
}


static inline bool tcp_ack_is_dubious(const struct sock *sk, const int flag)
{
	return !(flag & FLAG_NOT_DUP) || (flag & FLAG_CA_ALERT) ||
		inet_csk(sk)->icsk_ca_state != TCP_CA_Open;
}

/* Decide wheather to run the increase function of congestion control. */
static inline bool tcp_may_raise_cwnd(const struct sock *sk, const int flag)
{
	/* If reordering is high then always grow cwnd whenever data is
	 * delivered regardless of its ordering. Otherwise stay conservative
	 * and only grow cwnd on in-order delivery (RFC5681). A stretched ACK w/
	 * new SACK or ECE mark may first advance cwnd here and later reduce
	 * cwnd in tcp_fastretrans_alert() based on more states.
	 */
	if (tcp_sk(sk)->reordering > sock_net(sk)->ipv4.sysctl_tcp_reordering)
		return flag & FLAG_FORWARD_PROGRESS;

	return flag & FLAG_DATA_ACKED;
}

/* The "ultimate" congestion control function that aims to replace the rigid
 * cwnd increase and decrease control (tcp_cong_avoid,tcp_*cwnd_reduction).
 * It's called toward the end of processing an ACK with precise rate
 * information. All transmission or retransmission are delayed afterwards.
 */
static void tcp_cong_control(struct sock *sk, u32 ack, u32 acked_sacked,
		int flag, const struct rate_sample *rs)
{
	const struct inet_connection_sock *icsk = inet_csk(sk);

	if (icsk->icsk_ca_ops->cong_control) {
		icsk->icsk_ca_ops->cong_control(sk, rs);
		return;
	}

	if (tcp_in_cwnd_reduction(sk)) {
		/* Reduce cwnd if state mandates */
		tcp_cwnd_reduction(sk, acked_sacked, flag);
	} else if (tcp_may_raise_cwnd(sk, flag)) {
		/* Advance cwnd if state allows */
		tcp_cong_avoid(sk, ack, acked_sacked);
	}
	tcp_update_pacing_rate(sk);
}

/* Check that window update is acceptable.
 * The function assumes that snd_una<=ack<=snd_next.
 */
static inline bool tcp_may_update_window(const struct tcp_sock *tp,
		const u32 ack, const u32 ack_seq,
		const u32 nwin)
{
	return	after(ack, tp->snd_una) ||
		after(ack_seq, tp->snd_wl1) ||
		(ack_seq == tp->snd_wl1 && nwin > tp->snd_wnd);
}


static inline void tcp_in_ack_event(struct sock *sk, u32 flags)
{
	const struct inet_connection_sock *icsk = inet_csk(sk);

	if (icsk->icsk_ca_ops->in_ack_event)
		icsk->icsk_ca_ops->in_ack_event(sk, flags);
}

/* Congestion control has updated the cwnd already. So if we're in
 * loss recovery then now we do any new sends (for FRTO) or
 * retransmits (for CA_Loss or CA_recovery) that make sense.
 */
static void tcp_xmit_recovery(struct sock *sk, int rexmit)
{
	return;
}

/* This routine deals with incoming acks, but not outgoing ones. */
static int tcp_ack(struct sock *sk, const struct sk_buff *skb, int flag)
{


	STUB(1, "the stub function is called !\n");
	/*struct inet_connection_sock *icsk = inet_csk(sk);*/
	/*struct tcp_sock *tp = tcp_sk(sk);*/
	/*struct tcp_sacktag_state sack_state;*/
	/*struct rate_sample rs = { .prior_delivered = 0 };*/
	/*u32 prior_snd_una = tp->snd_una;*/
	/*u32 ack_seq = TCP_SKB_CB(skb)->seq;*/
	/*u32 ack = TCP_SKB_CB(skb)->ack_seq;*/
	/*bool is_dupack = false;*/
	/*u32 prior_fackets;*/
	/*int prior_packets = tp->packets_out;*/
	/*u32 delivered = tp->delivered;*/
	/*u32 lost = tp->lost;*/
	/*int acked = 0; [> Number of packets newly acked <]*/
	/*int rexmit = REXMIT_NONE; [> Flag to (re)transmit to recover losses <]*/

	/*sack_state.first_sackt.v64 = 0;*/
	/*sack_state.rate = &rs;*/

	/*[> We very likely will need to access write queue head. <]*/
	/*prefetchw(sk->sk_write_queue.next);*/

	/* If the ack is older than previous acks
	 * then we can probably ignore it.
	 */
	/*if (before(ack, prior_snd_una)) {*/
	/*[> RFC 5961 5.2 [Blind Data Injection Attack].[Mitigation] <]*/
	/*if (before(ack, prior_snd_una - tp->max_window)) {*/
	/*tcp_send_challenge_ack(sk, skb);*/
	/*return -1;*/
	/*}*/
	/*goto old_ack;*/
	/*}*/

	/* If the ack includes data we haven't sent yet, discard
	 * this segment (RFC793 Section 3.9).
	 */
	/*if (after(ack, tp->snd_nxt))*/
	/*goto invalid_ack;*/

	/*if (icsk->icsk_pending == ICSK_TIME_LOSS_PROBE)*/
	/*tcp_rearm_rto(sk);*/

	/*if (after(ack, prior_snd_una)) {*/
	/*flag |= FLAG_SND_UNA_ADVANCED;*/
	/*icsk->icsk_retransmits = 0;*/
	/*}*/

	/*prior_fackets = tp->fackets_out;*/
	/*rs.prior_in_flight = tcp_packets_in_flight(tp);*/

	/* ts_recent update must be made after we are sure that the packet
	 * is in window.
	 */
	/*if (flag & FLAG_UPDATE_TS_RECENT)*/
	/*tcp_replace_ts_recent(tp, TCP_SKB_CB(skb)->seq);*/

	/*if (!(flag & FLAG_SLOWPATH) && after(ack, prior_snd_una)) {*/
	/* Window is constant, pure forward advance.
	 * No more checks are required.
	 * Note, we use the fact that SND.UNA>=SND.WL2.
	 */
	/*tcp_update_wl(tp, ack_seq);*/
	/*tcp_snd_una_update(tp, ack);*/
	/*flag |= FLAG_WIN_UPDATE;*/

	/*tcp_in_ack_event(sk, CA_ACK_WIN_UPDATE);*/

	/*NET_INC_STATS(sock_net(sk), LINUX_MIB_TCPHPACKS);*/
	/*} else {*/
	/*u32 ack_ev_flags = CA_ACK_SLOWPATH;*/

	/*if (ack_seq != TCP_SKB_CB(skb)->end_seq)*/
	/*flag |= FLAG_DATA;*/
	/*else*/
	/*NET_INC_STATS(sock_net(sk), LINUX_MIB_TCPPUREACKS);*/

	/*flag |= tcp_ack_update_window(sk, skb, ack, ack_seq);*/

	/*if (TCP_SKB_CB(skb)->sacked)*/
	/*flag |= tcp_sacktag_write_queue(sk, skb, prior_snd_una,*/
	/*&sack_state);*/

	/*if (tcp_ecn_rcv_ecn_echo(tp, tcp_hdr(skb))) {*/
	/*flag |= FLAG_ECE;*/
	/*ack_ev_flags |= CA_ACK_ECE;*/
	/*}*/

	/*if (flag & FLAG_WIN_UPDATE)*/
	/*ack_ev_flags |= CA_ACK_WIN_UPDATE;*/

	/*tcp_in_ack_event(sk, ack_ev_flags);*/
	/*}*/

	/* We passed data and got it acked, remove any soft error
	 * log. Something worked...
	 */
	/*sk->sk_err_soft = 0;*/
	/*icsk->icsk_probes_out = 0;*/
	/*tp->rcv_tstamp = tcp_time_stamp;*/
	/*if (!prior_packets)*/
	/*goto no_queue;*/

	/*[> See if we can take anything off of the retransmit queue. <]*/
	/*flag |= tcp_clean_rtx_queue(sk, prior_fackets, prior_snd_una, &acked,*/
	/*&sack_state);*/

	/*if (tcp_ack_is_dubious(sk, flag)) {*/
	/*is_dupack = !(flag & (FLAG_SND_UNA_ADVANCED | FLAG_NOT_DUP));*/
	/*tcp_fastretrans_alert(sk, acked, is_dupack, &flag, &rexmit);*/
	/*}*/
	/*if (tp->tlp_high_seq)*/
	/*tcp_process_tlp_ack(sk, ack, flag);*/

	/*if ((flag & FLAG_FORWARD_PROGRESS) || !(flag & FLAG_NOT_DUP))*/
	/*sk_dst_confirm(sk);*/

	/*if (icsk->icsk_pending == ICSK_TIME_RETRANS)*/
	/*tcp_schedule_loss_probe(sk);*/
	/*delivered = tp->delivered - delivered;	[> freshly ACKed or SACKed <]*/
	/*lost = tp->lost - lost;			[> freshly marked lost <]*/
	/*tcp_rate_gen(sk, delivered, lost, sack_state.rate);*/
	/*tcp_cong_control(sk, ack, delivered, flag, sack_state.rate);*/
	/*tcp_xmit_recovery(sk, rexmit);*/
	/*return 1;*/

	/*no_queue:*/
	/*[> If data was DSACKed, see if we can undo a cwnd reduction. <]*/
	/*if (flag & FLAG_DSACKING_ACK)*/
	/*tcp_fastretrans_alert(sk, acked, is_dupack, &flag, &rexmit);*/
	/* If this ack opens up a zero window, clear backoff.  It was
	 * being used to time the probes, and is probably far higher than
	 * it needs to be for normal retransmission.
	 */
	/*if (tcp_send_head(sk))*/
	/*tcp_ack_probe(sk);*/

	/*if (tp->tlp_high_seq)*/
	/*tcp_process_tlp_ack(sk, ack, flag);*/
	/*return 1;*/

	/*invalid_ack:*/
	/*SOCK_DEBUG(sk, "Ack %u after %u:%u\n", ack, tp->snd_una, tp->snd_nxt);*/
	/*return -1;*/

	/*old_ack:*/
	/* If data was SACKed, tag it and see if we should send more data.
	 * If data was DSACKed, see if we can undo a cwnd reduction.
	 */
	/*if (TCP_SKB_CB(skb)->sacked) {*/
	/*flag |= tcp_sacktag_write_queue(sk, skb, prior_snd_una,*/
	/*&sack_state);*/
	/*tcp_fastretrans_alert(sk, acked, is_dupack, &flag, &rexmit);*/
	/*tcp_xmit_recovery(sk, rexmit);*/
	/*}*/

	/*SOCK_DEBUG(sk, "Ack %u before %u:%u\n", ack, tp->snd_una, tp->snd_nxt);*/
	return 0;
}


/* Check segment sequence number for validity.
 *
 * Segment controls are considered valid, if the segment
 * fits to the window after truncation to the window. Acceptability
 * of data (and SYN, FIN, of course) is checked separately.
 * See tcp_data_queue(), for example.
 *
 * Also, controls (RST is main one) are accepted using RCV.WUP instead
 * of RCV.NXT. Peer still did not advance his SND.UNA when we
 * delayed ACK, so that hisSND.UNA<=ourRCV.WUP.
 * (borrowed from freebsd)
 */

static inline bool tcp_sequence(const struct tcp_sock *tp, u32 seq, u32 end_seq)
{
	return	!before(end_seq, tp->rcv_wup) &&
		!after(seq, tp->rcv_nxt + tcp_receive_window(tp));
}

/* When we get a reset we do this. */
void tcp_reset(struct sock *sk)
{
	STUB(1, "the stub function is called !\n");
	return;
}


#endif /* ifndef simple_tcp_input */
