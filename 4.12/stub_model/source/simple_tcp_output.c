/*************************************************************************
    > File Name: simple_tcp_output.c
  > Author: wsun
  > Mail:sunweiflyus@gmail.com 
  > Created Time: Sat 24 Jun 2017 11:48:51 PM CDT
  > Comments: 
 ************************************************************************/
#ifndef simple_tcp_output
#define simple_tcp_output

/* This function synchronize snd mss to current pmtu/exthdr set.

   tp->rx_opt.user_mss is mss set by user by TCP_MAXSEG. It does NOT counts
   for TCP options, but includes only bare TCP header.

   tp->rx_opt.mss_clamp is mss negotiated at connection setup.
   It is minimum of user_mss and mss received with SYN.
   It also does not include TCP options.

   inet_csk(sk)->icsk_pmtu_cookie is last pmtu, seen by this function.

   tp->mss_cache is current effective sending mss, including
   all tcp options except for SACKs. It is evaluated,
   taking into account current pmtu, but never exceeds
   tp->rx_opt.mss_clamp.

   NOTE1. rfc1122 clearly states that advertised MSS
   DOES NOT include either tcp or ip options.

   NOTE2. inet_csk(sk)->icsk_pmtu_cookie and tp->mss_cache
   are READ ONLY outside this function.		--ANK (980731)
 */
unsigned int tcp_sync_mss(struct sock *sk, u32 pmtu)
{
	return 0;
}
EXPORT_SYMBOL(tcp_sync_mss);

#endif /* ifndef simple_tcp_output
 */

