/*************************************************************************
  > File Name:main_cubic_multip_ack.c
  > Author: wsun
  > Mail:sunweiflyus@gmail.com
  > Created Time: Wed 28 Jun 2017 07:04:52 PM CDT
  > Comments: for type3, cubic  multiple acks(within one RTT)
 ************************************************************************/
#include "share.h"
#include "tcp_cubic.c"

extern struct tcp_congestion_ops cubictcp;

int main(){

	struct tcp_sock tcp_sock_struct ;
	struct tcp_sock* sk = &tcp_sock_struct;
	struct bictcp *ca = inet_csk_ca(sk);
	struct tcp_sock *tp = tcp_sk(sk);
	struct inet_connection_sock *icsk = inet_csk(sk);

	cubictcp_register();
	memset(&tcp_sock_struct, 0, sizeof(struct tcp_sock));
	tcp_set_congestion_control(sk, &cubictcp);
	struct net net_namespace;
	memset(&net_namespace, 0, sizeof(struct net));
	sk->inet_conn.icsk_inet.sk.sk_net.net=&net_namespace;
	tcp_sk_init(&net_namespace);//namespace config: net for the sk

	printf("[Before initialization] tp->snd_cwnd:%u, tp->snd_ssthresh:%u\n", tp->snd_cwnd, tp->snd_ssthresh);

	tcp_init_sock(sk);
	bictcp_init(sk);
	tcp_ca_event(sk, CA_EVENT_TX_START);// a new event added in new kernel
	printf("[After initialization] tp->snd_cwnd:%u, tp->snd_ssthresh:%u\n", tp->snd_cwnd, tp->snd_ssthresh);

	//Initial symbolic state in the CA stage:
	//sym_cwnd_0: a concrete value of total number of packets within one RTT(now is 10 packets)
	//sym_ssth_0: symbolic value with a condtion less than sym_cwnd_0
	//sym_rtt_0: now just a conerete value of 100 ms (later may change to symbolic); note: HZ=1000 in kernel.h

	tcp_set_ca_state(sk, TCP_CA_Open);

	u32 sym_cwnd_0 = 0;
	klee_make_symbolic(&sym_cwnd_0, sizeof(u32), "sym_cwnd_0");
	tp->snd_cwnd = sym_cwnd_0;

	u32 sym_ssth_0 = 0;
	klee_make_symbolic(&sym_ssth_0, sizeof(u32), "sym_ssth_0");
	tp->snd_ssthresh = sym_ssth_0;
	
	u32 sym_rtt_0 = 100;
	//Below varaibles are from CA reset function to simulate CA history;
	u32 sym_last_max_cwnd_0 = 0;
	klee_make_symbolic(&sym_last_max_cwnd_0, sizeof(u32), "sym_last_max_cwnd_0");
	ca->last_max_cwnd = sym_last_max_cwnd_0;

	u32 sym_delay_min_0 = 0;
	klee_make_symbolic(&sym_delay_min_0, sizeof(u32), "sym_delay_min_0");
	ca->delay_min = sym_delay_min_0;

	u32 sym_epoch_start_0 = 0;
	klee_make_symbolic(&sym_epoch_start_0, sizeof(u32), "sym_epoch_start_0");
	ca->epoch_start = sym_epoch_start_0;

	u32 sym_ack_cnt_0 = 0;
	klee_make_symbolic(&sym_ack_cnt_0, sizeof(u32), "sym_ack_cnt_0");
	ca->ack_cnt = sym_ack_cnt_0;


	// Current time
	u32 sym_tcptimestamp_0 = 0;//To represent inital time;
	klee_make_symbolic(&sym_tcptimestamp_0, sizeof(u32), "sym_tcptimestamp_0");
	jiffies = sym_tcptimestamp_0;


	//Feasible constraints
	klee_assume(sym_cwnd_0 != 0);
	klee_assume(sym_ssth_0 != 0);
	klee_assume(tp->snd_cwnd >= tp->snd_ssthresh); //To limit in CA stage
	tp->is_cwnd_limited = 1; // Only consider unlimited case
	klee_assume(sym_last_max_cwnd_0 != 0);
	klee_assume(sym_delay_min_0 != 0);
	klee_assume(sym_ack_cnt_0 != 0);
	klee_assume(sym_epoch_start_0 != 0);
	klee_assume(sym_tcptimestamp_0 > sym_epoch_start_0); //epoach_start cannot later than current time


	// Begin a simulation of receiving 10 ACKs in CA stage
	struct ack_sample sample = { .pkts_acked = 2,
		.rtt_us = 100000,// =100 ms
		.in_flight = 10};

	for (int i = 0; i < 10; i ++)//For each packet
	{
		icsk->icsk_ca_ops->pkts_acked(sk, &sample); //To call pkts_acked() API

		u32 ack = 1;//* the ACK num in header is set to 1, which doesnot matter FOR CA*/ 
		u32 acked = 2;//* the number of ACKed packets is set to 2*/ 

		tcp_cong_avoid(sk, ack, acked);// To call cong_avoid() API

		jiffies += 10 ;//the time interval between ACKs is 10 ms
	}

	//Test Oracle
	if (ca->cnt < 2)
	{
		klee_warning(" ca->cnt < 2 !");
		klee_print_range("ca->cnt", ca->cnt);
	}

	return 0;
}
