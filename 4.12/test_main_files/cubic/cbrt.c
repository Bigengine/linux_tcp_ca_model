/*************************************************************************
  > File Name: cbrt.c
  > Author: wsun
  > Mail:sunweiflyus@gmail.com
  > Created Time: Wed 28 Jun 2017 09:42:31 AM CDT
  > Comments:To test different implementations of cubic rooot
 ************************************************************************/
#include "share.h"

/* calculate the cubic root of x using a table lookup followed by one
 * Newton-Raphson iteration.
 * Avg err ~= 0.195%
 * For latest kernel 4.12 since 2.6.22
 */
static u32 cubic_root(u64 a)
{
	u32 x, b, shift;
	/*
	 * cbrt(x) MSB values for x MSB values in [0..63].
	 * Precomputed then refined by hand - Willy Tarreau
	 *
	 * For x in [0..63],
	 *   v = cbrt(x << 18) - 1
	 *   cbrt(x) = (v[x] + 10) >> 6
	 */
	static const u8 v[] = {
		/* 0x00 */    0,   54,   54,   54,  118,  118,  118,  118,
		/* 0x08 */  123,  129,  134,  138,  143,  147,  151,  156,
		/* 0x10 */  157,  161,  164,  168,  170,  173,  176,  179,
		/* 0x18 */  181,  185,  187,  190,  192,  194,  197,  199,
		/* 0x20 */  200,  202,  204,  206,  209,  211,  213,  215,
		/* 0x28 */  217,  219,  221,  222,  224,  225,  227,  229,
		/* 0x30 */  231,  232,  234,  236,  237,  239,  240,  242,
		/* 0x38 */  244,  245,  246,  248,  250,  251,  252,  254,
	};

	b = fls64(a);
	if (b < 7) {
		/* a in [0..63] */
		return ((u32)v[(u32)a] + 35) >> 6;
	}

	b = ((b * 84) >> 8) - 1;
	shift = (a >> (b * 3));

	x = ((u32)(((u32)v[shift] + 10) << b)) >> 6;

	/*
	 * Newton-Raphson iteration
	 *                         2
	 * x    = ( 2 * x  +  a / x  ) / 3
	 *  k+1          k         k
	 */
	x = (2 * x + (u32)div64_u64(a, (u64)x * (u64)(x - 1)));
	x = ((x * 341) >> 10);
	return x;
}

/*
 * calculate the cubic root of x using Newton-Raphson
 * for kernel before 2.6.21
 */

static u32 cubic_root_21(u64 a)
{
	u32 x, x1;

	/* Initial estimate is based on:
	 * cbrt(x) = exp(log(x) / 3)
	 */
	x = 1u << (fls64(a)/3);

	/*
	 * Iteration based on:
	 *                         2
	 * x    = ( 2 * x  +  a / x  ) / 3
	 *  k+1          k         k
	 */
	do {
		x1 = x;
		x = (2 * x + (u32) div64_u64(a, x*x)) / 3;
	} while (abs(x1 - x) > 1);

	return x;
}

/*
 * calculate the cubic root of x using Newton-Raphson
 * for kernel 2.6.22
 */
static u32 cubic_root_22(u64 a)
{
	u32 x;

	/* Initial estimate is based on:
	 * cbrt(x) = exp(log(x) / 3)
	 */
	x = 1u << (fls64(a)/3);

	/* converges to 32 bits in 3 iterations */
	x = (2 * x + (u32)div64_u64(a, (u64)x*(u64)x)) / 3;
	x = (2 * x + (u32)div64_u64(a, (u64)x*(u64)x)) / 3;
	x = (2 * x + (u32)div64_u64(a, (u64)x*(u64)x)) / 3;

	return x;
}

/*
 * calculate the cubic root of x using Hacker's delight cube root algorithm
 * from the discussion about cubic optimizaion in https://groups.google.com/forum/#!msg/fa.linux.kernel/bWEMC-I2UKM/v98Gt1UhAVQJ
 * it seems the performance is good but CPU bounded
 */

static u32 cubic_root_hacker(u64 x)
{
	int s;
	u32 y;
	u64 b;
	u64 bs;

	y = 0;
	for (s = 63; s >= 0; s -= 3) {
		y = 2 * y;
		b = 3 * y * (y+1) + 1;
		bs = b << s;
		if (x >= bs && (b == (bs>>s))) {  /* avoid overflow */
			x -= bs;
			y++;
		}
	}
	return y;
}

int main ( int argc, char *argv[] )
{
	u64 sym_num = 0;
	klee_make_symbolic(&sym_num, sizeof(u64), "sym_num");
	klee_assume(sym_num !=0);
//	printf("a:%u, cubic of a:%u, cubic_21 of a:%u, cubic_22 of a:%u, cubic_hacker of a:%u \n", a, cubic_root(a), cubic_root_21(a), cubic_root_22(a), cubic_root_hacker(a));

	u32 res1 =  cubic_root(sym_num);
	u32 res2 =  cubic_root_22(sym_num);
	u32 diff = 0;
	if (res1 > res2)
	{
	   diff = res1 - res2;
	}
	else
	{
	   diff = res2 - res1;
	}

	if (diff > (res2 >> 5)) // 3.125%
	{
	  klee_warning("error difference > 3.125%");
	  klee_print_range("sym_num", sym_num);
	  klee_assume(0);
	}
	else
	{
	  klee_warning("finish !");
	}
}
