if [ $# -eq 1 ]
then
   echo "run the klee script: $1"
else
   echo "invalid argument please pass one argument: the binary name"
   exit 1;
fi

klee --only-output-states-covering-new -max-solver-time=30 -silent-klee-assume $1

#klee --search=dfs --only-output-states-covering-new -max-solver-time=10 --use-forked-solver --silent-klee-assume tcp_cubic.bc
#sudo klee-stats --print-all run.stats . | grep States
