/*************************************************************************
    > File Name: undo scenario
  > Author: wsun
  > Mail:sunweiflyus@gmail.com
  > Created Time: Wed 28 Jun 2017 04:04:52 PM CDT
  > Comments: 3 sym vars in total
 ************************************************************************/


#include "share.h"
#include "tcp_htcp.c"

extern struct tcp_congestion_ops htcp;

int main()
{
	struct tcp_sock tcp_sock_struct ;
	struct tcp_sock* sk = &tcp_sock_struct;
	struct htcp *ca = inet_csk_ca(sk);
	struct tcp_sock *tp = tcp_sk(sk);
	struct inet_connection_sock *icsk = inet_csk(sk);

	htcp_register();
	memset(&tcp_sock_struct, 0, sizeof(struct tcp_sock));
	tcp_set_congestion_control(sk, &htcp);
	struct net net_namespace;
	memset(&net_namespace, 0, sizeof(struct net));
	sk->inet_conn.icsk_inet.sk.sk_net.net = &net_namespace;
	tcp_sk_init(&net_namespace);//namespace config: net for the sk

	printf("[Before initialization] tp->snd_cwnd:%u, tp->snd_ssthresh:%u\n", tp->snd_cwnd, tp->snd_ssthresh);

	tcp_init_sock(sk);

	if (icsk->icsk_ca_ops->init)
		icsk->icsk_ca_ops->init(sk);
	tcp_ca_event(sk, CA_EVENT_TX_START);// a new event added in new kernel


	printf("[After initialization] tp->snd_cwnd:%u, tp->snd_ssthresh:%u\n", tp->snd_cwnd, tp->snd_ssthresh);

	//Initial Symbolic State:
	//sym_cwnd_0: symbolic range (1, 0xffffffff)
	//sym_ssth_0: symbolic range (1, 0xffffffff)
	u32 sym_cwnd_0 = 0;
	klee_make_symbolic(&sym_cwnd_0, sizeof(u32), "sym_cwnd_0");
	tp->snd_cwnd = sym_cwnd_0;

	u32 sym_ssth_0 = 0;
	klee_make_symbolic(&sym_ssth_0, sizeof(u32), "sym_ssth_0");
	tp->snd_ssthresh = sym_ssth_0;

	//Feasible constraints
	klee_assume(sym_cwnd_0 != 0);
	/*klee_assume(sym_cwnd_0 > 3);*/
	klee_assume(sym_ssth_0 != 0);


	//Begin a simulation of cwnd_reduction and then undo

	tcp_set_ca_state(sk, TCP_CA_Disorder);
	//step 1: cwnd_reduction
	klee_warning("step1: cwnd_reduction");
	tp->prior_ssthresh = tcp_current_ssthresh(sk);
	tp->snd_ssthresh = icsk->icsk_ca_ops->ssthresh(sk);

	tcp_set_ca_state(sk, TCP_CA_Recovery);
	u32 sym_cwnd_1 = 0;// cwnd_1: to simulate the cwnd reduced by "fast recover"
	klee_make_symbolic(&sym_cwnd_1, sizeof(u32), "sym_cwnd_1");
	tp->snd_cwnd = sym_cwnd_1;
	klee_assume(sym_cwnd_1 != 0);
	klee_assume(sym_cwnd_1 < sym_cwnd_0);// the new cwnd cannot be larger than the previous cwnd after a correct cwnd_reduction phase

	//step 2: undo the "cwnd_reduction"
	klee_warning("step2: undo the \"cwnd_reduction\"");
	tp->snd_cwnd = icsk->icsk_ca_ops->undo_cwnd(sk);


	//Test Oracle
	if (tp->snd_cwnd != sym_cwnd_0)
	{
		klee_warning("undo to a wrong cwnd !");
	}
	
	return 0;
}
