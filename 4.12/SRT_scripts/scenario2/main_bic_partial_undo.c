/*************************************************************************
    > File Name: partial undo scenario
  > Author: wsun
  > Mail:sunweiflyus@gmail.com
  > Created Time: Wed 28 Jun 2017 04:04:52 PM CDT
  > Comments: 9 sym vars in total
 ************************************************************************/
#include "share.h"
#include "tcp_bic.c"

extern struct tcp_congestion_ops bictcp;

int main()
{
	struct tcp_sock tcp_sock_struct ;
	struct tcp_sock* sk = &tcp_sock_struct;
	struct bictcp *ca = inet_csk_ca(sk);
	struct tcp_sock *tp = tcp_sk(sk);
	struct inet_connection_sock *icsk = inet_csk(sk);

	bictcp_register();
	memset(&tcp_sock_struct, 0, sizeof(struct tcp_sock));
	tcp_set_congestion_control(sk, &bictcp);
	struct net net_namespace;
	memset(&net_namespace, 0, sizeof(struct net));
	sk->inet_conn.icsk_inet.sk.sk_net.net = &net_namespace;
	tcp_sk_init(&net_namespace);//namespace config: net for the sk

	printf("[Before initialization] tp->snd_cwnd:%u, tp->snd_ssthresh:%u\n", tp->snd_cwnd, tp->snd_ssthresh);

	tcp_init_sock(sk);

	if (icsk->icsk_ca_ops->init)
		icsk->icsk_ca_ops->init(sk);
	tcp_ca_event(sk, CA_EVENT_TX_START);// a new event added in new kernel


	printf("[After initialization] tp->snd_cwnd:%u, tp->snd_ssthresh:%u\n", tp->snd_cwnd, tp->snd_ssthresh);

	//Initial Symbolic State:
	//sym_cwnd_0: symbolic range (1, 0xffffffff)
	//sym_ssth_0: symbolic range (1, 0xffffffff)
	u32 sym_cwnd_0 = 0;
	klee_make_symbolic(&sym_cwnd_0, sizeof(u32), "sym_cwnd_0");
	tp->snd_cwnd = sym_cwnd_0;

	u32 sym_ssth_0 = 0;
	klee_make_symbolic(&sym_ssth_0, sizeof(u32), "sym_ssth_0");
	tp->snd_ssthresh = sym_ssth_0;

	jiffies = 0; //inital time is set to 0

	//Feasible constraints
	klee_assume(sym_cwnd_0 != 0);
	klee_assume(sym_ssth_0 != 0);

	//Begin a simulation of cwnd_reduction and then undo
	tcp_set_ca_state(sk, TCP_CA_Disorder);

	//step 1: cwnd_reduction
	klee_warning("step1: cwnd_reduction");
	u32 sym_jiffies_0 = 0; // symbolic time to simulate the current time when a loss event occurs
	klee_make_symbolic(&sym_jiffies_0, sizeof(u32), "sym_jiffies_0");
	klee_assume(sym_jiffies_0 > 0);
	jiffies = sym_jiffies_0;

	tp->prior_ssthresh = tcp_current_ssthresh(sk);
	tp->snd_ssthresh = icsk->icsk_ca_ops->ssthresh(sk);

	tcp_set_ca_state(sk, TCP_CA_Recovery);
	u32 sym_cwnd_1 = 0;// cwnd_1: to simulate the cwnd reduced by "fast recover"
	klee_make_symbolic(&sym_cwnd_1, sizeof(u32), "sym_cwnd_1");
	tp->snd_cwnd = sym_cwnd_1;
	klee_assume(sym_cwnd_1 != 0);
	klee_assume(sym_cwnd_1 <= sym_cwnd_0);// the new cwnd cannot be larger than the previous cwnd after a correct cwnd_reduction phase

	//step 2: partial undo the "cwnd_reduction"
	klee_warning("step2: undo the \"cwnd_reduction\"");
	u32 sym_jiffies_1 = 0;//symbolic time to simulate the current time when "undo"
	klee_make_symbolic(&sym_jiffies_1, sizeof(u32), "sym_jiffies_1");
	klee_assume(sym_jiffies_1 > sym_jiffies_0);
	jiffies = sym_jiffies_1;
	tp->snd_cwnd = icsk->icsk_ca_ops->undo_cwnd(sk);

	//step 3: go to open state when exiting TCP_CA_Recovery
	klee_warning("step3: exits TCP_CA_Recovery\"");
	u32 sym_jiffies_2 = 0;//symbolic time to simulate the current time when "exits TCP_CA_Recovery"
	klee_make_symbolic(&sym_jiffies_2, sizeof(u32), "sym_jiffies_2");
	klee_assume(sym_jiffies_2 > sym_jiffies_1);
	jiffies = sym_jiffies_2;
	tcp_set_ca_state(sk, TCP_CA_Open);

	u32 sym_in_flight_0 = 0;//sym_in_flight_0: to simulate the in_flight of the ACK0
	klee_make_symbolic(&sym_in_flight_0, sizeof(u32), "sym_in_flight_0");//could be 0
	klee_assume(sym_in_flight_0 > 0);

	u32 sym_rtt_us_0 = 0;//sym_rtt_us_0: to simulate the rtt_us of the ACK0
	klee_make_symbolic(&sym_rtt_us_0, sizeof(u32), "sym_rtt_us_0");
	klee_assume(sym_rtt_us_0 > 0);

	struct ack_sample sample = { .pkts_acked = 2,
		       .rtt_us = sym_rtt_us_0,
		        .in_flight = sym_in_flight_0
	};

	icsk->icsk_ca_ops->pkts_acked(sk, &sample); //To call pkts_acked() API

	u32 sym_cwnd_cnt_0 = 0;//sym_cwnd_cnt_0: to simulate the current cwnd cnt for TCP
	klee_make_symbolic(&sym_cwnd_cnt_0, sizeof(u32), "sym_cwnd_cnt_0");
	klee_assume(sym_cwnd_cnt_0 > 0);
	tp->snd_cwnd_cnt = sym_cwnd_cnt_0;

	tp->is_cwnd_limited = 1;
	tcp_cong_avoid(sk, 2, 2);// To call cong_avoid() API to update current state

	//Test Oracle, to be added by user
	if (ca->epoch_start != 0 && ca->epoch_start != sym_jiffies_2)//For cubic family, need to call bictcp_update() to update before checking
	{
		klee_warning("The time of last loss event is incorrect !");
	}

	return 0;
}
